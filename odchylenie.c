#include "naglowkowy.h"
#include <math.h>
float odchylenie(float *tab, float sr, int x)
{
    double z=0.0;
    for(int i=0;i<x;i++){
        z+=pow(tab[i]-sr, 2);
    }
    double wariacja=z/50;
    double sd=sqrt(wariacja);
    float odchylenie=(double)sd;
    return odchylenie;
}

