#include "naglowkowy.h"
#include <math.h>

float mediana(float *tab, int x)
{
    float mediana=0;
    float z=0;

    for(int i=0;i<x-1;i++)
    {
        for(int j=0;j<x-1-i;j++)
        {
            if(tab[j]>tab[j+1])
            {
                z=tab[j];
                tab[j]=tab[j+1];
                tab[j+1]=z;
            }
        }
    }
    if(x%2==0)
    {
        mediana=tab[(x-1)/2]+tab[x/2];
        mediana=mediana/2;
    }
    else
    {
        mediana=tab[x/2];
    }
    return mediana;
}
