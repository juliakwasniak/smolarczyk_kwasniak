CC=gcc
CLAGS=-Wall
LDLIBS=-lm

main_2: main_2.o srednia.o odchylenie.o mediana.o 
	$(CC) $(CFLAGS) -o main_2 main_2.o srednia_2.o odchylenie_2.o mediana_2.o  $(LDLIBS)

main_2.o: main_2.c naglowkowy.h
	$(CC) $(CFLAGS) -c main_2.c

srednia.o: srednia.c
	$(CC) $(CFLAGS) -c srednia.c

odchylenie.o: odchylenie.c
	$(CC) $(CFLAGS) -c odchylenie.c

mediana.o: mediana.c 
	$(CC) $(CFLAGS) -c mediana.c 
