#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "naglowkowy.h"

void wyswietlanie(float *iks, float *igrek, float *erhao, int x)
{
    printf("LP\tX\t\tY\t\tRHO\n");
    for(int i=0; i<x; i++)
    {
        printf("%d\t%.5f\t\t%.5f\t%.3f\n",i+1, iks[i],igrek[i], erhao[i]);
    }
    return;

}
void wczytywanie(float *iks, float *igrek, float *erhao, int x)
{
    FILE *pliczek;
    pliczek = fopen("P0001_attr.rec", "r+");
    int tab[50];
    if(pliczek == NULL)
    {
        printf("ERROR");
        exit(1);
    }
    fseek(pliczek, 13, SEEK_SET);
    for(int i=0; i<x; i++)
    {
        fscanf(pliczek,"%d.\t%f \t%f\t%f\n", &tab[i], &iks[i],&igrek[i], &erhao[i]);
    }
    fclose(pliczek);
    return;
}

int main()
{
    FILE*pliczek;
    int n=50;
    float srX=0,srY=0,srR=0,oX=0,oY=0,oR=0,mX=0,mY=0,mR=0;
    float *iks=calloc(n, sizeof(float));
    float *igrek=calloc(n, sizeof(float));
    float *erhao=calloc(n, sizeof(float));

    wczytywanie(iks,igrek,erhao,n);
    wyswietlanie(iks,igrek,erhao,n);
//SREDNIA
    srX=srednia(iks,n);
    srY=srednia(igrek,n);
    srR=srednia(erhao,n);
//ODCHYLENIE
    oX=odchylenie(iks,srX,n);
    oY=odchylenie(igrek,srY,n);
    oR=odchylenie(erhao,srR,n);
//MEDIANA
    mX=mediana(iks,n);
    mY=mediana(igrek,n);
    mR=mediana(erhao,n);

    printf("\nSrednia:X=%f\n\tY=%f\n\tRHO=%f, \nOchylenie standardowe:X=%f\n\tY=%f\n\tRHO=%f\nMediana:X=%f\n\tY=%f\n\tRHO=%f",srX,srY,srR,oX,oY,oR,mX,mY,mR);
    pliczek=fopen("P0001_attr.rec","ab");

    fprintf(pliczek,"\nSrednia:X=%f\n\tY=%f\n\tRHO=%f\nOdchylenie standardowe:X=%f\n\tY=%f\n\tRHO=%f\nMediana:X=%f\n\tY=%f\n\tRHO=%f",srX,srY,srR,oX,oY,oR,mX,mY,mR);
    fclose(pliczek);
    return 0;
}
